﻿(function (module) {
    module.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '__env', 
    function ($stateProvider, $urlRouterProvider, $locationProvider, __env) {
        // Redirect any unmatched url
        $urlRouterProvider.otherwise('/');

        $stateProvider
            // Acccount parent state
            .state('account', {
                abstract:true,
                templateUrl: 'app/components/account/index.html' + AppVersion
            })
            // App parent state
            .state('app', {
                abstract: true,
                templateUrl: 'app/shared/content/content.html' + AppVersion
            })
                // Login
            .state('login', {
                parent: 'account',
                url: '/account/login',
                templateUrl: 'app/components/account/login/_login.html' + AppVersion,
                data: { pageTitle: 'Login' },
                controller: 'loginController',
                controllerAs: 'vm',
                params: {
                    returnState: null,
                    resetPassword: null,
                    message: ''
                },
                resolve: __env.resolve([
                    'assets/pages/css/login.css' + AppVersion,
                    'app/components/account/login/loginController.js' + AppVersion
                ])
            })
            .state('home', {
                parent: 'app',
                url: '/',
                templateUrl: 'app/components/home/index.html' + AppVersion,
                data: { pageTitle: 'Home' },
                controller: 'homeController',
                controllerAs: 'vm',
                resolve: __env.resolve([
                    'app/components/home/homeController.js' + AppVersion
                ])    
            })
        // $locationProvider.html5Mode(true);
    }]);
    
}(angular.module('Epsilon')));
