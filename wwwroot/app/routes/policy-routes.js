﻿(function (module) {
    module.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '__env', 
    function ($stateProvider, $urlRouterProvider, $locationProvider, __env) {
        // Redirect any unmatched url
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('policy', {
                parent: 'app',
                url: '/policy',
                templateUrl: 'app/components/policy/index.html' + AppVersion,
                data: { pageTitle: 'Home' },
                controller: 'policyController',
                controllerAs: 'vm',
                resolve: __env.resolve([
                    'app/components/policy/policyController.js' + AppVersion,
                    'app/services/policyService.js' + AppVersion
                ])    
            })
            .state('policy-add', {
                parent: 'app',
                url: '/policy/add',
                templateUrl: 'app/components/policy/policyAdd.html' + AppVersion,
                data: { pageTitle: 'Add New Policy' },
                controller: 'policyAddController',
                controllerAs: 'vm',
                resolve: __env.resolve([
                    'app/components/policy/policyAddController.js' + AppVersion,
                    'app/services/policyService.js' + AppVersion,
                    'app/services/lookupService.js' + AppVersion
                ])
            })
            .state('policy-edit', {
                parent: 'app',
                url: '/policy/:id/edit',
                templateUrl: 'app/components/policy/policyEdit.html' + AppVersion,
                data: { pageTitle: 'Policy Details' },
                controller: 'policyEditController',
                controllerAs: 'vm',
                resolve: __env.resolve([
                    'app/components/policy/policyEditController.js' + AppVersion,
                    'app/services/policyService.js' + AppVersion,
                    'app/services/lookupService.js' + AppVersion,
                    'app/services/tableUtils.js' + AppVersion,
                ])
            })
            .state('policy-details', {
                parent: 'app',
                url: '/policy/:id',
                templateUrl: 'app/components/policy/policyDetails.html' + AppVersion,
                data: { pageTitle: 'Policy Details' },
                controller: 'policyDetailsController',
                controllerAs: 'vm',
                resolve: __env.resolve([
                    'app/components/policy/policyDetailsController.js' + AppVersion,
                    'app/components/policy/sublimits/memorandaController.js' + AppVersion,
                    'app/services/policyService.js' + AppVersion,
                    'app/services/lookupService.js' + AppVersion,
                    'app/services/tableUtils.js' + AppVersion,
                ]),
            })
            //.state('policy-details-tab', {
            //  parent: 'app',
            //  url: '/policy/:id/active/:tab',
            //  templateUrl: 'app/components/policy/policyDetails.html' + AppVersion,
            //  data: { pageTitle: 'Policy Details' },
            //  controller: 'policyDetailsController',
            //  controllerAs: 'vm',
            //  resolve: __env.resolve([
            //      'app/components/policy/policyDetailsController.js' + AppVersion,
            //      'app/services/policyService.js' + AppVersion,
            //      'app/services/lookupService.js' + AppVersion,
            //      'app/services/tableUtils.js' + AppVersion,
            //  ]),
            //})
            .state('policy-location-add', {
              parent: 'app',
              url: '/policies/:id/location/add',
              templateUrl: 'app/components/policy/locations/locationAdd.html' + AppVersion,
              data: { pageTitle: 'Add New Policy Location' },
              controller: 'policyLocationAddController',
              controllerAs: 'vm',
              resolve: __env.resolve([
                  'app/components/policy/locations/policyLocationAddController.js' + AppVersion,
                  'app/services/policyService.js' + AppVersion,
                  'app/services/lookupService.js' + AppVersion
              ])
            })
    }]);

}(angular.module('Epsilon')));
