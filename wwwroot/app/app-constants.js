﻿(function (module) {
    'use strict';
    var env = {};
    // Import variables if present (from env.js)
    if (window) {
        //Object.assign(env, window.__env);
        env = window.__env;

        env.resolve = function(files){
            return {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'Epsilon',
                        insertBefore: '#ng_load_plugins_before',
                        files: files
                    });
                }]
            }
        }
    }
    // Register environment in AngularJS as constant
    module.constant('__env', env);
    module.constant("epConstants", {

    }).run(function ($rootScope, epConstants) {
        $rootScope.constants = epConstants;
    });


}(angular.module('Epsilon')));
