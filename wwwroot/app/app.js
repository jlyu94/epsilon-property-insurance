﻿(function () {
    'use strict';
    angular.module('Epsilon', [
        'ui.router',
        'ui.bootstrap',
        'oc.lazyLoad',
        'ngSanitize',
        'ngJsTree',
        'ui.select',
        'flow',
        'angularMoment',
        'ui.tinymce',
        'ODataResources',
        'toastr',
        'ui.slimscroll',
        'ngStorage',
        'ngResource',
        'angular-ladda'
    ]);
}());