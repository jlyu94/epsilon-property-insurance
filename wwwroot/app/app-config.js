﻿(function (module) {

    /* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
    module.config(['$ocLazyLoadProvider', '$httpProvider', '$odataresourceProvider', 'toastrConfig', 'laddaProvider', function ($ocLazyLoadProvider, $httpProvider, $odataresourceProvider, toastrConfig, laddaProvider) {
        $ocLazyLoadProvider.config({
            // global configs go here
        });

        // Fix for IE Caching get requests
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }

        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

        $odataresourceProvider.defaults.actions['patch'] = { method: "PATCH" };
        angular.extend(toastrConfig, {
            preventOpenDuplicates: true,
        });
        laddaProvider.setOption({ /* optional */
            style: 'expand-right',
        });
    }]);

    // Configure Storage Provider prefix
    module.config(['$localStorageProvider',
    function ($localStorageProvider) {
        $localStorageProvider.setKeyPrefix('epsilon-');
    }])

    module.factory('sessionInjector', ['$q', '$rootScope', '$localStorage', function ($q, $rootScope, $localStorage) {

        function AccessToken() {
            return $localStorage.access_token;
        };

        var sessionInjector = {
            request: function (config) {
                if (AccessToken()) {
                    config.headers['Authorization'] = 'Bearer ' + AccessToken();
                }
                else {
                    delete config.headers['Authorization'];
                }
                return config;
            },
            responseError: function (response) {
                if (response.status == 401) {
                    // Clear access tokens
                    delete $localStorage.UserRole;
                    delete $rootScope.access_token;
                    delete $localStorage.access_token;
                    // Broadcast unauthorized request
                    $rootScope.$broadcast('unauthorized');
                }

                return $q.reject(response);
            }
        };
        return sessionInjector;
    }]);
    module.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('sessionInjector');
    }]);
    
    /* Setup global settings */
    module.factory('settings', ['$rootScope', function ($rootScope) {
      // supported languages
      var settings = {
        policy: {
          defaultTab: 0
        }
      };

      $rootScope.settings = settings;

      return settings;
    }]);


    module.run(['$rootScope', '$state', '$localStorage', 'authService', '$ocLazyLoad', '$timeout', '__env', '$location',
        function ($rootScope, $state, $localStorage, authService, $ocLazyLoad, $timeout, __env, $location) {
        $rootScope.AccessToken = function () {
            return $localStorage.access_token;
        }

        $rootScope.$on('unauthorized', function () {
            authService.logout();
            
            $rootScope.returnState = {
                name: $state.current.name,
                params: $state.params
            };
            $state.go("login"); // redirect to login page

        });
    }]);
}(angular.module('Epsilon')));
