﻿(function (module) {
    /***
    GLobal Directives
    ***/


    // Permission directive can be bound to DOM elements and directly references the Auth service 
    module.directive('authorized', ['authService', function (authService) {
        return {
            restrict: 'A',
            scope: {
                permission: '=',
                roles: '=',
            },

            link: function (scope, elem, attrs) {
                scope.$watch(function () { return authService.getCurrentUser(); }, function () {
                    if (authService.hasPermission(scope.permission) && authService.hasRoles(scope.roles)) {
                        elem.show();
                    } else {
                        elem.hide();
                    }
                }, true);
            }
        }
    }]);

    // Header directive
    module.directive('header', ['$rootScope', '$timeout', '$state', '$localStorage', 'authService', 'userService', '$window', function ($rootScope, $timeout, $state, $localStorage, authService, userService, $window) {
        return {
            restrict: 'EA',
            templateUrl: 'app/shared/header/header.html',
            link: function (scope, elem, attrs) {
                $timeout(function () {
                    Layout.initHeader(); // init header
                }, 500);
            }
        }
    }]);

    // Sidebar directive
    module.directive('sidebar', ['$timeout', '$localStorage', function ($timeout, authService, $localStorage) {
        return {
            restrict: 'EA',
            templateUrl: 'app/shared/sidebar/sidebar.html',
            link: function (scope, elem, attrs) {
                $timeout(function () {
                    Layout.initSidebar(); // init sidebar
                }, 500);
            }
        }
    }]);

    // Footer directive
    module.directive('footer', ['$timeout', function ($timeout) {
        return {
            restrict: 'EA',
            templateUrl: 'app/shared/footer/footer.html',
            link: function (scope, elem, attrs) {
                $timeout(function () {
                    Layout.initFooter(); // init footer
                }, 500);
            }
        }
    }]);

    //module.directive('counter', ['$rootScope', function ($rootScope) {
    //    return {
    //        restrict: "C",
    //        scope: {
    //            settings: '='
    //        },
    //        link: function (scope, element, attrs) {
    //            // Initialize counter up jquery plugin
    //            $(element).counterUp();
    //        }
    //    };
    //}]);

    // Bootstrap table directive
    module.directive('bt', ['authService', '$rootScope', function (authService, $rootScope) {
        return {
            restrict: "EA",
            scope: {
                settings: '='
            },
            link: function (scope, element, attrs) {

                var pageSize = 10;
                if (scope.settings.size) {
                    pageSize = scope.settings.size;
                };

                var authorizationHeader = {};
                if (authService.isLoggedIn()) {
                    authorizationHeader = { headers: { "Authorization": "Bearer " + authService.isLoggedIn() } };
                }
                if (scope.settings.ajaxOptions) {
                    authorizationHeader = scope.settings.ajaxOptions;
                }


                $(element).bootstrapTable({
                    cache: false,
                    url: scope.settings.url,
                    ajaxOptions: authorizationHeader,
                    responseHandler: scope.settings.responseHandler,
                    height: scope.settings.height,
                    striped: true,
                    onLoadSuccess: scope.settings.onLoadSuccess,
                    data: scope.settings.data,
                    showHeader: scope.settings.showHeader == null ? true : scope.settings.showHeader,
                    pagination: scope.settings.pagination == null ? true : scope.settings.pagination,
                    pageSize: pageSize,
                    pageList: [5, 10, 25, 50, 100, 200],
                    sidePagination: scope.settings.url ? 'server' : 'client',
                    queryParams: scope.settings.params,
                    onPostBody: scope.settings.onPostBody,
                    columns: scope.settings.columns,
                    onClickRow: scope.settings.events,
                    onLoadError: function (status, res) {
                        if (status == "401")
                            $rootScope.$broadcast('unauthorized');
                    }
                });
            }
        };
    }]);

    module.directive('popupDialog', ['$rootScope', function () {
        return {
            templateUrl: "templates/modal.html"
        }
    }]);

    // Route State Load Spinner(used on page or content load)
    module.directive('ngSpinnerBar', ['$rootScope',
    function ($rootScope) {
        return {
            link: function (scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                //$rootScope.$on('$stateChangeStart', function () {
                //    element.removeClass('hide'); // show spinner bar
                //});

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function () {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator

                    setTimeout(function () {
                        Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu
                    });
                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, 500);
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function () {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function () {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
    ])

    module.directive('spinnerMessage', function () {
        return {
            restrict: 'AE',
            template: '<span class="font-xs"><i class="fa fa-circle-o-notch fa-spin"></i>&nbsp;Saving...</span>',
        };
    });

    module.directive('validateEmail', function () {
        var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
        return {
            link: function (scope, elm) {
                elm.on("keyup", function () {
                    var isMatchRegex = EMAIL_REGEXP.test(elm.val());
                    if (isMatchRegex && elm.hasClass('border-red') || elm.val() == '') {
                        elm.removeClass('border-red');
                    } else if (isMatchRegex == false && !elm.hasClass('border-red')) {
                        elm.addClass('border-red');
                    }
                });
            }
        }
    });

    module.directive('myModal', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.dismiss = function () {
                    element.modal('hide');
                };
            }
        }
    });

    module.directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    module.directive('noType', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                event.preventDefault();
            });
        };
    });

    module.directive('tooltip', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                $(element).hover(function () {
                    // on mouseenter
                    $(element).tooltip('show');
                }, function () {
                    // on mouseleave
                    $(element).tooltip('hide');
                });
            }
        };
    });

    module.directive('stringToNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (value) {
                    return '' + value;
                });
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value, 10);
                });
            }
        };
    });

    module.directive('numbersOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9]/g, '');

                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    });

    module.directive('inputMoney', ['$timeout', function ($timeout) {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {

                String.prototype.replaceAll = function (search, replacement) {
                    var target = this;
                    return target.replace(new RegExp(search, 'g'), replacement);
                };

                var addCommas = attr['commas'] == null || attr['commas'] == 'true';

                element.on('blur', function () {
                    format();
                });

                $timeout(format, 10);

                function format() {
                    var value = ngModelCtrl.$modelValue;
                    if (isNaN(value))
                        value = value.replaceAll(",", "");

                    if (value) {
                        var newValue = (parseFloat(value).toFixed(2));

                        console.log(addCommas);
                        if (addCommas)
                            newValue = FormatNumber(newValue);

                        ngModelCtrl.$setViewValue(newValue);
                        ngModelCtrl.$render();
                    }
                }

                function FormatNumber(number) {
                    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
            }
        };
    }]);

    // Handle global LINK click
    module.directive('a', function () {
        return {
            restrict: 'E',
            scope: {
                enable: '='
            },
            link: function (scope, elem, attrs) {
                var enable = (scope.enable !== null || scope.enable !== undefined) ? scope.enable : true;

                if (!enable) {
                    if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                        elem.on('click', function (e) {
                            e.preventDefault();
                        });
                    }
                }
            }
        };
    });

    module.directive('datePicker', function () {
        return {
            // Restrict it to be an attribute in this case
            restrict: 'A',
            // responsible for registering DOM listeners as well as updating the DOM
            link: function (scope, element, attrs) {
                $(element).datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        };
    });

    // Weekday picker
    module.directive("weekDayPicker", function () {
        return {
            require: "ngModel",
            // Restrict it to be an attribute in this case
            restrict: 'E',
            templateUrl: 'app/directives/templates/weekdayPickerTmp.html',
            scope: {
                ngModel: '=',
                onChange: '&'
            },
            link: function (scope, element, attributes, ngModel) {

                if (attributes.hasOwnProperty('readonly')) {
                    scope.disabled = true;
                }

                scope.daysOfWeek = [
                  { name: 'Mon' },
                  { name: 'Tue' },
                  { name: 'Wed' },
                  { name: 'Thu' },
                  { name: 'Fri' },
                  { name: 'Sat' },
                  { name: 'Sun' },
                  { name: 'Hol' }
                ];

                scope.isActive = function (day) {
                    switch (day) {
                        case 'Mon':
                            return scope.ngModel.monday;
                        case 'Tue':
                            return scope.ngModel.tuesday;
                        case 'Wed':
                            return scope.ngModel.wednesday;
                        case 'Thu':
                            return scope.ngModel.thursday;
                        case 'Fri':
                            return scope.ngModel.friday;
                        case 'Sat':
                            return scope.ngModel.saturday;
                        case 'Sun':
                            return scope.ngModel.sunday;
                        case 'Hol':
                            return scope.ngModel.holiday;
                        default:
                            return false;
                    }
                };

                scope.setDay = function (day) {
                    if (!attributes.hasOwnProperty('readonly')) {
                        day.active = !day.active;
                        switch (day.name) {
                            case 'Mon':
                                scope.ngModel.monday = !scope.ngModel.monday;
                                break;
                            case 'Tue':
                                scope.ngModel.tuesday = !scope.ngModel.tuesday;
                                break;
                            case 'Wed':
                                scope.ngModel.wednesday = !scope.ngModel.wednesday;
                                break;
                            case 'Thu':
                                scope.ngModel.thursday = !scope.ngModel.thursday;
                                break;
                            case 'Fri':
                                scope.ngModel.friday = !scope.ngModel.friday;
                                break;
                            case 'Sat':
                                scope.ngModel.saturday = !scope.ngModel.saturday;
                                break;
                            case 'Sun':
                                scope.ngModel.sunday = !scope.ngModel.sunday;
                                break;
                            case 'Hol':
                                scope.ngModel.holiday = !scope.ngModel.holiday;
                                break;
                        }
                        scope.onChange(scope.ngModel);
                        ngModel.$setViewValue(scope.ngModel);
                        ngModel.$render();
                    }
                };
            }
        };
    });

    module.directive('timePicker', function () {
        return {
            // Restrict it to be an attribute in this case
            restrict: 'A',
            // responsible for registering DOM listeners as well as updating the DOM
            link: function (scope, element, attrs) {
                $(element).timepicker({
                    autoclose: true,
                    minuteStep: 10,
                    defaultTime: false,
                    showMeridian: (attrs.showmeridian == 'true' || attrs.showmeridian == null),
                });
            }
        };
    });

    module.directive('minicolors', function () {
        return {
            // Restrict it to be an attribute in this case
            restrict: 'A',
            // responsible for registering DOM listeners as well as updating the DOM
            link: function (scope, element, attrs) {

                $(element).minicolors({
                    theme: 'bootstrap'
                });
            }
        };
    });

    // Handle Dropdown Hover Plugin Integration
    module.directive('dropdownMenuHover', function () {
        return {
            link: function (scope, elem) {
                elem.dropdownHover();
            }
        };
    });



    // Used to compare fields in form validation (i.e. Confirm Password = Password)
    module.directive("compareTo", function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    });

    module.directive("copyFrom", function () {
        return {
            require: "ngModel",
            scope: {
                otherModel: "=copyFrom",
                copyIf: "="
            },
            link: function (scope, element, attributes, ngModel) {
                scope.$watch("copyIf", function (newValue) {
                    if (newValue == true) {
                        ngModel.$setViewValue(scope.otherModel);
                        ngModel.$render();
                    }
                });

                scope.$watch("otherModel", function (newValue) {
                    if (scope.copyIf) {
                        ngModel.$setViewValue(newValue);
                        ngModel.$render();
                    }
                });
            }
        };
    });

    // workaround directive to allow UniformJS to be ng-model bind values correctly
    //angular.module('EasyPayroll')
    //    .directive('input', function ($timeout) {
    //        return {
    //            restrict: 'E',
    //            require: '?ngModel',
    //            link: function (scope, element, attr, ngModel) {
    //                if (attr.type === 'checkbox' && attr.ngModel && attr.noUniform === undefined) {
    //                    element.uniform({ useID: false });
    //                    scope.$watch(function () { return ngModel.$modelValue }, function () {
    //                        $timeout(jQuery.uniform.update, 0);
    //                    });
    //                }
    //            }
    //        };
    //    });

    module.directive('enterKey', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.enterKey);
                    });

                    event.preventDefault();
                }
            });
        };
    });

    module.directive('strength', function () {
        return function (scope, element, attrs) {
            element.pwstrength({
                raisePower: 1.4,
                minChar: 8,
                verdicts: ["Weak", "Normal", "Medium", "Strong", "Very Strong"],
                scores: [17, 26, 40, 50, 60]
            });
        };
    });

    module.directive('fallbackSrc', function () {
        var fallbackSrc = {
            link: function postLink(scope, iElement, iAttrs) {
                iElement.bind('error', function () {
                    angular.element(this).attr("src", iAttrs.fallbackSrc);
                });
            }
        }
        return fallbackSrc;
    });

    module.directive('reqlabel', function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                var elementContent = $(element).html();
                var requireString = '<span style="color: red !important;">&nbsp;*</span>'
                var newContent = elementContent + requireString;

                $(element).html(newContent);
            }
        }
    });

    module.directive('clearHide', ['$timeout', function ($timeout) {
        // clears the value of the ng model if field is hidden or not required
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {
                ngModel: '=',
                ngRequired: '=',
                ngShow: '=',
                ngIf: '=',
                ngHide: '=',
            },
            link: function (scope, element, attrs, ngModel) {
                if (attrs.ngRequired) {
                    scope.$watch("ngRequired", function (newValue) {
                        if (newValue == false) {
                            ngModel.$setViewValue(null);
                            ngModel.$render();
                            ngModel.$setPristine();
                        }
                    }, true);
                } else if (attrs.ngShow) {
                    scope.$watch("ngShow", function (newValue) {
                        if (newValue == false) {
                            ngModel.$setViewValue(null);
                            ngModel.$render();
                            ngModel.$setPristine();
                        }
                    }, true);
                }
                else if (attrs.ngIf) {
                    scope.$watch("ngShow", function (newValue) {
                        if (newValue == false) {
                            ngModel.$setViewValue(null);
                            ngModel.$render();
                            ngModel.$setPristine();
                        }
                    }, true);
                }
                else if (attrs.ngHide) {
                    scope.$watch("ngHide", function (newValue) {
                        if (newValue == true) {
                            ngModel.$setViewValue(null);
                            ngModel.$render();
                            ngModel.$setPristine();
                        }
                    }, true);
                }
            }
        };
    }]);

    module.directive('numericOnly', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, modelCtrl) {

                modelCtrl.$parsers.push(function (inputValue) {
                    var transformedInput = inputValue ? inputValue.replace(/[^0-9]/, '') : null;

                    if (transformedInput != inputValue) {
                        modelCtrl.$setViewValue(transformedInput);
                        modelCtrl.$render();
                    }

                    return transformedInput;
                });
            }
        };
    });

    module.directive('dateEarlierThan', [
    function () {

        var link = function ($scope, $element, $attrs, ctrl) {

            var validate = function (viewValue) {
                var comparisonModel = $attrs.dateEarlierThan;

                if (!viewValue || !comparisonModel) {
                    // It's valid because we have nothing to compare against
                    ctrl.$setValidity('dateEarlierThan', true);
                }
                else {
                    var startValid = moment(viewValue, "DD/MM/YYYY").isValid();
                    var endValid = moment(comparisonModel, "DD/MM/YYYY").isValid();
                    if (startValid == true && endValid == true) {
                        ctrl.$setValidity('dateEarlierThan', (moment(comparisonModel, "DD/MM/YYYY").isAfter(moment(viewValue, "DD/MM/YYYY"), 'day')));

                    }
                    else {
                        ctrl.$setValidity('dateEarlierThan', true);
                    }
                }

                return viewValue;
            };

            ctrl.$parsers.unshift(validate);
            ctrl.$formatters.push(validate);

            $attrs.$observe('dateEarlierThan', function (comparisonModel) {
                // Whenever the comparison model changes we'll re-validate
                return validate(ctrl.$viewValue);
            });

        };

        return {
            require: 'ngModel',
            link: link
        };

    }
    ]);

    module.directive('customTimeValidation', [
    function () {

        var link = function ($scope, $element, $attrs, ctrl) {

            $element.on('blur', function () {
                validate(ctrl.$viewValue);
            });

            var validate = function (viewValue) {
                if (!viewValue) {
                    // It's valid because we have nothing to compare against
                    ctrl.$setValidity('isValidTime', true);
                }
                else {
                    viewValue = viewValue.toUpperCase(); //Force any characters (am/pm) to uppercase for ease of checking

                    //Accepts the following formats (24H):
                    //HH:MM
                    //HH.MM
                    //HHMM
                    //H:MM
                    //H.MM
                    //HHM -- This is not supposed to be valid. It should be HMM instead. A special validation will be needed when dealing with HHM time
                    //H:M
                    //H.M
                    //HM -- This is not supposed to be valid. It should be HH instead. A special validation will be needed when dealing with HM time
                    //H
                    var format24HGeneral = /^(2[0-3]|[01]?[0-9])[:\.]?([0-5]?[0-9])$/;

                    //Accepts the following formats (12H AM/PM):
                    //HH:MM AM/PM
                    //HH.MM AM/PM
                    //HHMM AM/PM
                    //H:MM AM/PM
                    //H.MM AM/PM
                    //HHM AM/PM -- This is not supposed to be valid. It should be HMM AM/PM instead. A special validation will be needed when dealing with HHM time
                    //H:M AM/PM
                    //H.M AM/PM
                    //HM AM/PM -- This is not supposed to be valid. It should be HH AM/PM instead. A special validation will be needed when dealing with HM time
                    //H AM/PM
                    var format12HGeneral = /^(1[0-2]|0?[1-9])[:\.]?([0-5]?[0-9])( ?[AP][M]?)$/;

                    //Accepts the following formats (24H):
                    //HH
                    var format24HHrsOnly = /^(2[0-3]|[01]?[0-9])$/;

                    //Accepts the following formats (12H AM/PM):
                    //HH AM/PM
                    var format12HHrsOnly = /^(1[0-2]|0?[1-9])( ?[AP][M]?)$/;

                    //Accepts the following formats (24H):
                    //H:MM
                    var format24HHrsMin = /^([0]?[0-9])[:\.]?(([0-5][0-9])|([0-5]))$/;

                    //Accepts the following formats (12H AM/PM):
                    //H:MM AM/PM
                    var format12HHrsMin = /^(0?[1-9])[:\.]?(([0-5][0-9])|([0-5]))( ?[AP][M]?)$/;

                    var matched24HGeneral = viewValue.match(format24HGeneral);
                    var matched24HHrsOnly = viewValue.match(format24HHrsOnly);
                    var matched24HHrsMin = viewValue.match(format24HHrsMin);

                    var matched12HGeneral = viewValue.match(format12HGeneral);
                    var matched12HHrsOnly = viewValue.match(format12HHrsOnly);
                    var matched12HHrsMin = viewValue.match(format12HHrsMin);

                    var format24HGeneralValid = matched24HGeneral != null ? true : false;
                    var format24HHrsOnlyValid = matched24HHrsOnly != null ? true : false;
                    var format24HHrsMinValid = matched24HHrsMin != null ? true : false;

                    var format12HGeneralValid = matched12HGeneral != null ? true : false;
                    var format12HHrsOnlyValid = matched12HHrsOnly != null ? true : false;
                    var format12HHrsMinValid = matched12HHrsMin != null ? true : false;

                    if (format24HGeneralValid == false && format24HHrsOnlyValid == false && format24HHrsMinValid == false && format12HGeneralValid == false && format12HHrsOnlyValid == false && format12HHrsMinValid == false) {
                        ctrl.$setValidity('isValidTime', false);
                        viewValue = null;
                    }
                    else {
                        ctrl.$setValidity('isValidTime', true);
                    }

                    if (format24HHrsOnlyValid == true) {
                        var hrsParsed = parseInt(matched24HHrsOnly[1]);

                        if (hrsParsed > 12) {
                            hrsParsed -= 12;
                            viewValue = (hrsParsed + ':00 PM');
                        }
                        else {
                            viewValue = (hrsParsed + ':00 AM');
                        }

                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format24HHrsMinValid == true) {
                        var mins = matched24HHrsMin[2];

                        if (mins.length < 2) {
                            mins += '0';
                        }

                        viewValue = (matched24HHrsMin[1] + ':' + mins + ' AM');
                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format24HGeneralValid == true) {
                        if (matched24HGeneral[1].length + matched24HGeneral[2].length > 3) {
                            var hrsParsed = parseInt(matched24HGeneral[1]);

                            var mins = matched24HGeneral[2];
                            if (mins.length < 2) {
                                mins += '0';
                            }

                            if (hrsParsed > 12) {
                                hrsParsed -= 12;
                                viewValue = (hrsParsed + ':' + mins + ' PM');
                            }
                            else {
                                viewValue = (hrsParsed + ':' + mins + ' AM');
                            }

                            ctrl.$setViewValue(viewValue);
                            ctrl.$render();
                            return viewValue;
                        }
                        else {
                            ctrl.$setValidity('isValidTime', false);
                        }
                    }


                    if (format12HHrsOnlyValid == true) {
                        viewValue = matched12HHrsOnly[1].trim() + ':00 ' + matched12HHrsOnly[2].trim();

                        if (matched12HHrsOnly[2].trim().length < 2) {
                            viewValue += 'M';
                        }

                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format12HHrsMinValid == true) {
                        var mins = matched12HHrsMin[2].trim();
                        if (mins.length < 2) {
                            mins += '0';
                        }

                        viewValue = (matched12HHrsMin[1].trim() + ':' + mins + ' ' + matched12HHrsMin[5].trim());

                        if (matched12HHrsMin[5].trim().length < 2) {
                            viewValue += 'M';
                        }

                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format12HGeneralValid == true) {
                        if (matched12HGeneral[1].length + matched12HGeneral[2].length > 2) {

                            viewValue = (matched12HGeneral[1].trim() + ':' + matched12HGeneral[2].trim() + ' ' + matched12HGeneral[3].trim());

                            if (matched12HGeneral[3].trim().length < 2) {
                                viewValue += 'M';
                            }

                            ctrl.$setViewValue(viewValue);
                            ctrl.$render();
                            return viewValue;
                        }
                        else {
                            ctrl.$setValidity('isValidTime', false);
                        }
                    }
                }

                ctrl.$setViewValue(viewValue);
                ctrl.$render();
                return viewValue;
            };

            //Removed in favor of using $element.on('blur')

            //ctrl.$parsers.unshift(validate);
            //ctrl.$formatters.push(validate);

            //$attrs.$observe('isValidTime', function (viewValue) {
            //    // Whenever the comparison model changes we'll re-validate
            //    return validate(viewValue);
            //});
        };

        return {
            require: 'ngModel',
            link: link
        };

    }
    ]);

    module.directive('customTimeValidationEnd', [
    function () {

        var link = function ($scope, $element, $attrs, ctrl) {

            $element.on('blur', function () {
                validate(ctrl.$viewValue);
            });

            var validate = function (viewValue) {

                if (!viewValue) {
                    // It's valid because we have nothing to compare against
                    ctrl.$setValidity('isValidTime', true);
                }
                else {
                    viewValue = viewValue.toUpperCase(); //Force any characters (am/pm) to uppercase for ease of checking
                    //Accepts the following formats (24H):
                    //HH:MM
                    //HH.MM
                    //HHMM
                    //H:MM
                    //H.MM
                    //HHM -- This is not supposed to be valid. It should be HMM instead. A special validation will be needed when dealing with HHM time
                    //H:M
                    //H.M
                    //HM -- This is not supposed to be valid. It should be HH instead. A special validation will be needed when dealing with HM time
                    //H
                    var format24HGeneral = /^(2[0-3]|[01]?[0-9])[:\.]?([0-5]?[0-9])$/;

                    //Accepts the following formats (12H AM/PM):
                    //HH:MM AM/PM
                    //HH.MM AM/PM
                    //HHMM AM/PM
                    //H:MM AM/PM
                    //H.MM AM/PM
                    //HHM AM/PM -- This is not supposed to be valid. It should be HMM AM/PM instead. A special validation will be needed when dealing with HHM time
                    //H:M AM/PM
                    //H.M AM/PM
                    //HM AM/PM -- This is not supposed to be valid. It should be HH AM/PM instead. A special validation will be needed when dealing with HM time
                    //H AM/PM
                    var format12HGeneral = /^(1[0-2]|0?[1-9])[:\.]?([0-5]?[0-9])( ?[AP][M]?)$/;

                    //Accepts the following formats (24H):
                    //HH
                    var format24HHrsOnly = /^(2[0-3]|[01]?[0-9])$/;

                    //Accepts the following formats (12H AM/PM):
                    //HH AM/PM
                    var format12HHrsOnly = /^(1[0-2]|0?[1-9])( ?[AP][M]?)$/;

                    //Accepts the following formats (24H):
                    //H:MM
                    var format24HHrsMin = /^([0]?[0-9])[:\.]?(([0-5][0-9])|([0-5]))$/;

                    //Accepts the following formats (12H AM/PM):
                    //H:MM AM/PM
                    var format12HHrsMin = /^(0?[1-9])[:\.]?(([0-5][0-9])|([0-5]))( ?[AP][M]?)$/;

                    var matched24HGeneral = viewValue.match(format24HGeneral);
                    var matched24HHrsOnly = viewValue.match(format24HHrsOnly);
                    var matched24HHrsMin = viewValue.match(format24HHrsMin);

                    var matched12HGeneral = viewValue.match(format12HGeneral);
                    var matched12HHrsOnly = viewValue.match(format12HHrsOnly);
                    var matched12HHrsMin = viewValue.match(format12HHrsMin);

                    var format24HGeneralValid = matched24HGeneral != null ? true : false;
                    var format24HHrsOnlyValid = matched24HHrsOnly != null ? true : false;
                    var format24HHrsMinValid = matched24HHrsMin != null ? true : false;

                    var format12HGeneralValid = matched12HGeneral != null ? true : false;
                    var format12HHrsOnlyValid = matched12HHrsOnly != null ? true : false;
                    var format12HHrsMinValid = matched12HHrsMin != null ? true : false;

                    if (format24HGeneralValid == false && format24HHrsOnlyValid == false && format24HHrsMinValid == false && format12HGeneralValid == false && format12HHrsOnlyValid == false && format12HHrsMinValid == false) {
                        ctrl.$setValidity('isValidTime', false);
                    }
                    else {
                        ctrl.$setValidity('isValidTime', true);
                    }

                    if (format24HHrsOnlyValid == true) {
                        var hrsParsed = parseInt(matched24HHrsOnly[1]);

                        if (hrsParsed > 12) {
                            hrsParsed -= 12;
                            viewValue = (hrsParsed + ':00 PM');
                        }
                        else {
                            viewValue = (hrsParsed + ':00 PM');
                        }

                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format24HHrsMinValid == true) {
                        var mins = matched24HHrsMin[2];

                        if (mins.length < 2) {
                            mins += '0';
                        }

                        viewValue = (matched24HHrsMin[1] + ':' + mins + ' AM');
                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format24HGeneralValid == true) {
                        if (matched24HGeneral[1].length + matched24HGeneral[2].length > 3) {
                            var hrsParsed = parseInt(matched24HGeneral[1]);

                            var mins = matched24HGeneral[2];
                            if (mins.length < 2) {
                                mins += '0';
                            }

                            if (hrsParsed > 12) {
                                hrsParsed -= 12;
                                viewValue = (hrsParsed + ':' + mins + ' PM');
                            }
                            else {
                                viewValue = (hrsParsed + ':' + mins + ' AM');
                            }

                            ctrl.$setViewValue(viewValue);
                            ctrl.$render();
                            return viewValue;
                        }
                        else {
                            ctrl.$setValidity('isValidTime', false);
                        }
                    }


                    if (format12HHrsOnlyValid == true) {
                        viewValue = matched12HHrsOnly[1].trim() + ':00 ' + matched12HHrsOnly[2].trim();

                        if (matched12HHrsOnly[2].trim().length < 2) {
                            viewValue += 'M';
                        }

                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format12HHrsMinValid == true) {
                        var mins = matched12HHrsMin[2].trim();
                        if (mins.length < 2) {
                            mins += '0';
                        }

                        viewValue = (matched12HHrsMin[1].trim() + ':' + mins + ' ' + matched12HHrsMin[5].trim());

                        if (matched12HHrsMin[5].trim().length < 2) {
                            viewValue += 'M';
                        }

                        ctrl.$setViewValue(viewValue);
                        ctrl.$render();
                        return viewValue;
                    }

                    if (format12HGeneralValid == true) {
                        if (matched12HGeneral[1].length + matched12HGeneral[2].length > 2) {

                            viewValue = (matched12HGeneral[1].trim() + ':' + matched12HGeneral[2].trim() + ' ' + matched12HGeneral[3].trim());

                            if (matched12HGeneral[3].trim().length < 2) {
                                viewValue += 'M';
                            }

                            ctrl.$setViewValue(viewValue);
                            ctrl.$render();
                            return viewValue;
                        }
                        else {
                            ctrl.$setValidity('isValidTime', false);
                        }
                    }
                }

                return viewValue;
            };

            //ctrl.$parsers.unshift(validate);
            //ctrl.$formatters.push(validate);

            //$attrs.$observe('isValidTime', function (viewValue) {
            //    // Whenever the comparison model changes we'll re-validate
            //    return validate(ctrl.$viewValue);
            //});
        };

        return {
            require: 'ngModel',
            link: link
        };

    }
    ]);

    module.directive('iframeOnload', [function () {
        return {
            scope: {
                callBack: '&iframeOnload',
            },
            link: function (scope, element, attrs) {
                element.on('load', function () {
                    var result = scope.callBack();
                    scope.$apply();
                    return result;
                })
            }
        }
    }])

    module.directive('compile', function ($compile, $parse) {
        return {
            restrict: 'E',
            link: function (scope, element, attr) {
                scope.$watch(attr.content,
                    function () {
                        element.html($parse(attr.content)(scope));
                        $compile(element.contents())(scope);
                    },
                    true);
            }
        }
    });

    module.directive('onLoadElement', ['$timeout', function ($timeout) {
      return {
        scope: {
          onLoadElement: '=',
        },
        link: function (scope, element, attrs) {
          $timeout(function () {
            console.log(scope);
            if (scope.onLoadElement) {
              var $element = document.getElementById(attrs.id);

              if ($element.tagName == 'VIDEO') {
                $element.addEventListener("loadedmetadata", function () {
                  var result = scope.onLoadElement($element.videoHeight, $element.videoWidth);
                  scope.$apply();
                  return result;
                });
              } else {
                $element.addEventListener("load", function () {
                  var result = scope.onLoadElement($element.naturalHeight, $element.naturalWidth);
                  scope.$apply();
                  return result;
                });
              }
            }
          });
        }
      }
    }]);

    module.directive('inlineEdit', function () {
      return {
        restrict: 'A',
        transclude: true,
        template: '<label class="editing" data-ng-transclude></label>',
        scope: {
          defaultClass: "@defaultClass"
        },
        controller: ['$scope', '$element', '$transclude', function ($scope, $element, $transclude) {
          $transclude(function (clone) {
            $scope.transcluded_content = clone[0] != undefined && clone[0] != null && clone[0].length > 0 ? clone[0].textContent : '';
          });
          $element.bind('click', function () {
            var editableType = $element[0].getAttribute('data-type');
            var editableSource = $element[0].getAttribute('data-source');
            var sourceDisplay = editableSource != 'summary' ? 'sublimit' : editableSource;
            var contentForDisplay = $element[0].textContent;
            if (editableType === 'textarea') {
              $element.hide().after('<textarea class="form-control ' + sourceDisplay + '-container summary-textarea" type="text">' + contentForDisplay + '</textarea>');
            } else {
              $element.hide().after('<input class="form-control ' + sourceDisplay + '-container" type="text" value="' + contentForDisplay + '" />');
            }


            $element.next().focus().blur(function () {
              $scope.transcluded_content = $element.next().val();
              $element.html($scope.transcluded_content);
              $element.next().remove();
              $element.show();
            });
          });

        }]
      };
    });

}(angular.module('Epsilon')));
