(function (module) {
    'use strict';

    module.factory('lookupService', ['$http', '__env', function ($http, __env) {

        var url = __env.apiUrl + '/lookup/'

        var get = function (key, query) {
            return $http.get(url + key, { params: { query: query } });
        };

        return {
            get: get,
        }

    }]);

}(angular.module('Epsilon')));


