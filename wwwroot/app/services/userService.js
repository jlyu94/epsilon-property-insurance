﻿(function (module) {
    'use strict';

    module.factory('userService', ['$http', '$httpParamSerializer', '__env', function ($http, $httpParamSerializer, __env) {
            
        var search = function (user) {
            return $http.get(__env.apiUrl + 'api/user', {
                params: user
            })
        };

        var find = function (id) {
            return $http.get(__env.apiUrl + 'api/user/' + id)
        };

        var update = function (user) {
            return $http.put(__env.apiUrl + 'api/user', user)
        }

        var create = function (user) {
            return $http.post(__env.apiUrl + 'api/user', user);
        }

        var getCurrentUser = function () {
            return $http.get(__env.apiUrl + 'api/user/current');
        }

        var getTreeData = function (id) {
            return $http.get(__env.apiUrl + 'api/user/permissions/' + id);
        }

        var updateSignature = function (signatureId) {
            return $http.put(__env.apiUrl + 'api/user/signature/' + signatureId);
        }

        var validateEmail = function (email) {
            return $http.get(__env.apiUrl + 'api/user/validateEmail', {
                params: { email: email }
            })
        };

        var setClientID = function (object) {
            return $http.post(ApiURL + 'api/account/set-client/', object);
        };

        var resetPassword = function (object) {
            return $http.post(ApiURL + 'api/account/reset-password', object);
        }

        var confirmResetPassword = function (object) {
            return $http.post(ApiURL + 'api/account/confirm-reset-password', object);
        }

        var setPersona = function (object) {
            return $http.post(ApiURL + 'api/user/set-persona/', object);
        };

        return {
            search: search,
            find: find,
            update: update,
            create: create,
            getCurrentUser: getCurrentUser,
            getTreeData: getTreeData,
            updateSignature: updateSignature,
            validateEmail: validateEmail,
            setPersona: setPersona
        }

    }]);

}(angular.module('Epsilon')));
