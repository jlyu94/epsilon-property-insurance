﻿(function (module) {
    'use strict';

    module.factory('tableUtilsService', ['$filter', function ($filter) {
        var tableUtilsService = {
            dateFormatter: dateFormatter,
            dateTimeFormatter: dateTimeFormatter,
            defaultFormatter: defaultFormatter,
            currencyFormatter: currencyFormatter,
        };

        function currencyFormatter(value, row, index) {
            return value ? $filter('currency')(Math.floor(value), '$', 0) : '$0'
        }
        function percentageFormatter(value, row, index) {
            return value ? $filter('number')(value, 2) + '%' : ''
        }
        function defaultFormatter(value, row, index) {
            return value || '';
        }
        function dateFormatter(value, row, index) {
            return value ? $filter('date')(value, "d/M/yyyy") : '';
        }
        function dateTimeFormatter(value, row, index) {
            return value ? $filter('date')(value, "d/M/yyyy h:mm a") : '';
        }

        return tableUtilsService;
    }]);

})(angular.module('Epsilon'));