﻿(function (module) {
    'use strict';

    module.factory('accountService', ['$http', '__env', function ($http, __env) {

        

        var resetPassword = function (object) {
            return $http.post(ApiURL + 'api/account/reset-password', object);
        }

        var confirmResetPassword = function (object) {
            return $http.post(ApiURL + 'api/account/confirm-reset-password', object);
        }

        var initConfirmResetPassword = function (object) {
            var config = {
                params: object
            }
            return $http.get(ApiURL + 'api/account/confirm-reset-password', config);
        };

        var initEmployeeInvitation = function (object) {
            var config = {
                params: object
            }
            return $http.get(ApiURL + 'api/account/employee-invitation', config);
        }

        var acceptEmployeeInvitation = function (object) {
            return $http.post(ApiURL + 'api/account/employee-invitation', object);
        }

        var initClientUserInvitation = function (object) {
            var config = {
                params: object
            }

            console.log(config);
            return $http.get(ApiURL + 'api/account/client-user-invitation', config);
        }

        var acceptClientUserInvitation = function (object) {
            return $http.post(ApiURL + 'api/account/client-user-invitation', object);
        }

        var acceptAdminUserInvitation = function (object) {
            return $http.post(ApiURL + 'api/account/admin-invitation', object);
        }

        var initUserInvitation = function (object) {
            var config = {
                params: object
            }

            return $http.get(ApiURL + 'api/account/user-invitation', config);
        }

        var acceptUserInvitation = function (object) {
            return $http.post(ApiURL + 'api/account/user-invitation', object);
        }

        return {
            resetPassword: resetPassword,
            initConfirmResetPassword: initConfirmResetPassword,
            confirmResetPassword: confirmResetPassword,
            initEmployeeInvitation: initEmployeeInvitation,
            acceptEmployeeInvitation: acceptEmployeeInvitation,
            initClientUserInvitation: initClientUserInvitation,
            acceptClientUserInvitation: acceptClientUserInvitation,
            acceptAdminUserInvitation: acceptAdminUserInvitation,
            initUserInvitation: initUserInvitation,
            acceptUserInvitation: acceptUserInvitation
        }
    }]);

}(angular.module('Epsilon')));