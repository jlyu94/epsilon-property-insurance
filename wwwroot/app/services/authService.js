﻿(function (module) {
    module.factory('authService', ['$rootScope', '$resource', '$sessionStorage', '$localStorage', '$httpParamSerializer', '$q', '__env', '$ocLazyLoad', '$http',
      function ($rootScope, $resource, $sessionStorage, $localStorage, $httpParamSerializer, $q, __env, $ocLazyLoad, $http) {

          var _currentUser = $sessionStorage.user || {};

          var service = {
              init: _init,
              currentUser: _currentUser,
              getCurrentUser: getCurrentUser,
              isHRManager: isHRManager,
              isContractManager: isContractManager,
              getIdentity: _getIdentity,
              checkPermissionForView: _checkPermissionForView,
              hasPermission: _hasPermission,
              hasRoles: _hasRoles,
              isLoggedIn: _isLoggedIn,
              logout: _logout,
              login: _login,
              isTimesheetsEnabled: isTimesheetsEnabled,
              isOnboardingEnabled: isOnboardingEnabled,
              expireToken: expireToken
          };

          var User = $resource(__env.apiUrl.concat('api/user'), {}, {
              login: {
                  url: __env.apiUrl.concat('api/Token'),
                  method: "POST",
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                  transformRequest: function (data) {
                      return $httpParamSerializer(data);
                  },
                  isArray: false
              },
              getIdentity: {
                  method: 'GET',
                  url: __env.apiUrl.concat('api/user/identity')
              }
          });


          function _getIdentity() {
              return User.getIdentity().$promise
                  .then(function (data) {
                      // Do whatever when the request is finished

                      _currentUser = data;
                      $localStorage.UserRole = _currentUser.persona ? _currentUser.persona.entityName : null;
                      $localStorage.EntityID = _currentUser.persona ? _currentUser.persona.entityID : null;
                      $sessionStorage.user = _currentUser;
                      $localStorage.client_style = null;
                      $localStorage.client_path = null;

                      cssInjector.removeAll();
                      if (_currentUser.styleFileName) {
                          $localStorage.client_style = __env.apiContentUrl + _currentUser.styleFileName;
                          $localStorage.client_path = $localStorage.client_style + '?v=' + Date.now();
                          cssInjector.add($localStorage.client_path);
                      }

                      return data;
                  })
                  .catch(function (error) {
                      _logout();
                      throw error;
                  });
          };

          function _isLoggedIn() {
              return $localStorage.access_token;
              //return $localStorage.access_token && _currentUser.persona != null && _currentUser.persona.entityName;
          };

          function _init() {
              $rootScope.access_token = $localStorage.access_token;
              var deferred = $q.defer();
              if ($localStorage.access_token) {

                  return _getIdentity();

                  //if (angular.equals({}, _currentUser)) {
                  //} else {
                  //    deferred.resolve(_currentUser);
                  //    return deferred.promise;
                  //}
              } else {
                  deferred.reject("unauthorized");
                  return deferred.promise;
              }
          };

          function _login(username, password, domain) {
              var _data = {
                  username: username,
                  password: password,
                  grant_type: 'password'
              };

              if (domain)
                  _data.domain = domain;

              var deferred = $q.defer();
              User.login(_data).$promise
                  .then(function (data) {
                      $localStorage.access_token = data.access_token;
                      $rootScope.access_token = data.access_token;
                      return _getIdentity();
                  })
                  .then(function (data) {
                      deferred.resolve();
                  })
                  .catch(function (error) {
                      deferred.reject(error);
                  });
              return deferred.promise;
          };

          function expireToken() {
              return $http.post(__env.apiUrl + 'api/account/logout');
          }

          function _logout() {
                  delete $localStorage.UserRole;
                  delete $localStorage.access_token;
                  delete $sessionStorage.user;
                  delete $rootScope.access_token;
                  cssInjector.removeAll();
                  clearCurrentUser(); //Current user needs to be cleared whenever the user is logged out.
          };

          function _hasPermission(permission) {
              if (!_isLoggedIn()) {
                  return false;
              }

              if (!permission || !permission.length) {
                  return true;
              }

              return (_currentUser && _currentUser.permissions) ? _currentUser.permissions.indexOf(permission) >= 0 : false;
          };

          function _hasRoles(roles) {
              if (!_isLoggedIn()) {
                  return false;
              }

              if (!roles || !roles.length) {
                  return true;
              }

              var roleFound = false;
              if (_currentUser.persona) {
                  angular.forEach(roles, function (role, index) {
                      if (_currentUser.persona.entityName.indexOf(role) >= 0) {
                          roleFound = true;
                      }
                  });
              }

              // Else the user can have either role or permission to be authorized
              return roleFound;
          };

          function _checkPermissionForView(view) {
              var result = false;

              if (!view.authorize) {
                  result = true;
                  return result;
              }

              if (view.canAccess) {
                  result = view.canAccess(service) && hasPermissionForView(view);
                  return result;
              }

              result = hasPermissionForView(view);
              return result;
          };

          function hasPermissionForView(view) {
              if (!_isLoggedIn()) {
                  return false;
              }

              var hasRoles = _hasRoles(view.roles);
              var hasPermission = _hasPermission(view.permission);

              // If roles and permissions are specified, the customer should have both role and permission it them to be authorized.
              if (view.roles && view.permission) {
                  return hasRoles && hasPermission;
              }

              if (view.roles)
                  return hasRoles;

              if (view.permission)
                  return hasPermission;

              // Else the user can have either role or permission for the to be authorized
              return true;
          };

          function getCurrentUser() {
              return _currentUser;
          }

          function isHRManager() {
              if (_currentUser.persona) {
                  return _currentUser.persona.entityName == "ClientUser" && _currentUser.persona.isHRManager;
              }
              return false;
          }

          function isTimesheetsEnabled() {
              if (_currentUser.persona) {
                  return _currentUser.persona.isTimesheetsEnabled;
              }
              return false;
          }

          function isOnboardingEnabled() {
              if (_currentUser.persona) {
                  return _currentUser.persona.isOnboardingEnabled;
              }
              return false;
          }

          function isContractManager(contractManagerID) {
              if (_currentUser.persona) {
                  return _currentUser.persona.entityName == "ClientUser" && _currentUser.persona.entityID == contractManagerID;
              }
              return false;
          }

          function clearCurrentUser() {
              //Add additional logic pertaining to the current user here as necessary
              _currentUser = {};
          }


          

          return service;
      }
    ])
}(angular.module('Epsilon')));
