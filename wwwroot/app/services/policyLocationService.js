﻿(function (module) {
  'use strict';

  module.factory('policyLocationService', ['$http', '__env', function ($http, __env) {

    var url = __env.apiUrl + '/policyLocations/'

    var get = function (id) {
      if (id)
        return $http.get(url + id)
      return $http.get(url)
    };

    var post = function (data) {
      return $http.post(url, data);
    }

    var put = function (id, data) {
      return $http.put(url + id, data);
    }

    return {
      get: get,
      post: post,
      put: put,
    }

  }]);

}(angular.module('Epsilon')));


