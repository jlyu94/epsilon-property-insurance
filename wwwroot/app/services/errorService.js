﻿
(function () {
    'use strict';

    angular
      .module('Epsilon')
      .factory('errorService', ['toastr', errorService]);

    function errorService(toastr) {
        var service = {
            success: success,
            error: error,
            info: info,
            warning: warning,
        };

        function success(item) {
            if (typeof item == "string") {
                toastr.success(item);
            } else if (item.message) {
                toastr.success(item.message);
            } else if (item.data) {
                toastr.success(item.data);
            } else {
                toastr.success('Success');
            }
        }

        function error(item) {
            if (item.data) {
                if (typeof item.data == "string") {
                    toastr.error(item.data);
                }
                else if (item.data.error && item.data.error.message) {
                    toastr.error(item.data.error.message);
                }
                else if (item.data.message) {
                    toastr.error(item.data.message);
                }
            } else if (typeof item == "string") {
                toastr.error(item);
            } else if (item.message) {
                toastr.error(item.message);
            } else if (item.data) {
                toastr.error(item.data);
            } else if (item.error) {
                toastr.error(item.error);
            } else {
                toastr.error('An error occured');
            }
        }

        function info(item) {
            if (typeof item == "string") {
                toastr.info(item);
            } else if (item.message) {
                toastr.info(item.message);
            } else if (item.data) {
                toastr.info(item.data);
            }
        }

        function warning(item) {
            if (typeof item == "string") {
                toastr.warning(item);
            } else if (item.message) {
                toastr.warning(item.message);
            } else if (item.data) {
                toastr.warning(item.data);
            } else {
                toastr.warning('Warning');
            }
        }

        return service;
    }
})();
