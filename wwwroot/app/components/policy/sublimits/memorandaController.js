﻿(function (module) {
  'use strict';

  module.controller('memorandaController', ['$filter', '$uibModalInstance', '$http', '$scope', '$log', 'settings'
      , function ($filter, $uibModalInstance, $http, $scope, $log, settings) {
        var vm = this;
        vm.checkList = [
          { label: "1. Lorem ipsum", id: 1 },
          { label: "2. Lorem ipsum", id: 2 },
          { label: "3. Lorem ipsum", id: 3 },
          { label: "4. Lorem ipsum", id: 4 },
          { label: "5. Lorem ipsum", id: 5 },
          { label: "6. Extra Cost of Reinstatement", id: 6 },
          { label: "7. Additional Extra Cost of Reinstatement", id: 7 },
          { label: "8. Lorem ipsum", id: 8 },
          { label: "9. Lorem ipsum", id: 9 },
          { label: "10. Lorem ipsum", id: 10 },
          { label: "11. Lorem ipsum", id: 11 },
          { label: "12. Premises in the Vicinity (Prevention of Access)", id: 12 },
          { label: "13. Utilities", id: 13 },
          { label: "14. Unspecified Suppliers' and Customers' Premises - Australia", id: 14 },
          { label: "15. Closure of the Insured Premises by Order of a Public Authority", id: 15 },
          { label: "16. Lorem ipsum", id: 16 },
          { label: "17. Loss of Land Value", id: 17 },
          { label: "18. Landscaping", id: 18 },
          { label: "19. Accidental Damage (as defined in the Policy)", id: 19 }
        ];
        vm.cancel = cancel;
        vm.save = save;

        function cancel() {
          $uibModalInstance.dismiss('cancel');
        };

        function save() {
          $uibModalInstance.dismiss('save');
        };

      }]);
})(angular.module('Epsilon'));