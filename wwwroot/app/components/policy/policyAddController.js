﻿(function (module) {
    'use strict';

    module.controller('policyAddController', ['$rootScope', '$scope', '$state', '$timeout', 'authService', '$location', 'errorService', 'moment', '$localStorage', 'policyService', 'lookupService', policyAddController]);

    function policyAddController($rootScope, $scope, $state, $timeout, authService, $location, errorService, moment, $localStorage, policyService, lookupService) {
        var vm = this;

        vm.init = function () {
            vm.setInsured = setInsured;
            vm.setBroker = setBroker;
            vm.checkStatus = checkStatus;
            vm.searchInsured = searchInsured;
            vm.searchBroker = searchBroker;
            vm.disableReason = true;
            vm.hideReasonDiv = false;
            vm.save = save;
            vm.policy = {};
            getStatusLookup()
            getUnderwriterLookup()
            getTransactionTypeLookup();
            getReasonLookup();
        }

        function save() {
            $state.go('policy');
        }

        function getStatusLookup() {
            lookupService.get('status')
                .then(function (response) {
                    vm.statusList = response.data;
                    vm.policy.statusId = vm.statusList[0].id;
                    vm.hideReasonDiv = true;
                });
        }

        function getUnderwriterLookup() {
            lookupService.get('underwriter')
                .then(function (response) {
                    vm.underwriterList = response.data;
                });
        }

        function getTransactionTypeLookup() {
            lookupService.get('transactiontype')
                .then(function (response) {
                    vm.transactionTypeList = response.data;
                });
        }

        function getReasonLookup() {
            lookupService.get('reason')
                .then(function (response) {
                    vm.reasonList = response.data;
                });
        }

        function searchInsured(query) {
            lookupService.get('insured', query)
                .then(function (response) {
                    vm.insuredList = response.data;
                });
        }

        function searchBroker(query) {
            lookupService.get('broker', query)
                .then(function (response) {
                    vm.brokerList = response.data;
                });
        }

        function setInsured() {
            vm.insured = _.find(vm.insuredList, function (o) { return o.id == vm.policy.insuredId; });
        }

        function setBroker() {
            vm.broker = _.find(vm.brokerList, function (o) { return o.id == vm.policy.brokerId; });
        }

        function checkStatus() {
            vm.status = _.find(vm.statusList, function (o) { return o.id == vm.policy.statusId; });
            console.log(vm.status);
            vm.disableReason = vm.status.value.indexOf('Cancelled') < 0 && vm.status.value.indexOf('Declined') < 0;
            vm.hideReasonDiv = vm.disableReason;

            //clear reason if status is not cancelled/declined.
            if (vm.disableReason) {
                vm.policy.reasonId = null;
            }
        }

        $scope.$on('$viewContentLoaded', function () {
            vm.init();
        });
    }

})(angular.module('Epsilon'));
