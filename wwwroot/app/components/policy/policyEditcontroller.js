﻿(function (module) {
    'use strict';

    module.controller('policyEditController', ['$rootScope', '$scope', '$state', '$timeout', 'authService', '$location', 'errorService', 'moment', '$localStorage', '$stateParams', 'policyService', 'lookupService', 'tableUtilsService', policyEditController]);

    function policyEditController($rootScope, $scope, $state, $timeout, authService, $location, errorService, moment, $localStorage, $stateParams, policyService, lookupService, tableUtilsService) {
        var vm = this;

        vm.init = function () {
            vm.id = $stateParams.id;
            vm.setInsured = setInsured;
            vm.setBroker = setBroker;
            vm.checkStatus = checkStatus;
            vm.populateEndDate = populateEndDate;
            vm.searchInsured = searchInsured;
            vm.searchBroker = searchBroker;
            vm.isPolicyDetails = true;
            vm.disableReason = true;
            vm.hideReasonDiv = false;
            vm.save = save;
            getStatusLookup()
            getUnderwriterLookup()
            getTransactionTypeLookup();
            getReasonLookup();
            $timeout(function () {
                getPolicy();
            }, 1500);
            
        }

        function save() {
            $state.go('policy');
        }

        function getPolicy() {
            policyService.get(vm.id)
                .then(function (response) {
                    vm.policy = response.data;
                    vm.policy.periodOfCoverFrom = tableUtilsService.dateFormatter(vm.policy.periodOfCoverFrom);
                    vm.policy.periodOfCoverTo = tableUtilsService.dateFormatter(vm.policy.periodOfCoverTo);
                    vm.policy.effectivityFrom = tableUtilsService.dateFormatter(vm.policy.effectivityFrom);
                    vm.policy.effectivityTo = tableUtilsService.dateFormatter(vm.policy.effectivityTo);
                    setInsured();
                    setBroker();
                    checkStatus();
                });
        }

        function getStatusLookup() {
            lookupService.get('status')
                .then(function (response) {
                    vm.statusList = response.data;
                });
        }

        function getUnderwriterLookup() {
            lookupService.get('underwriter')
                .then(function (response) {
                    vm.underwriterList = response.data;
                });
        }

        function getTransactionTypeLookup() {
            lookupService.get('transactiontype')
                .then(function (response) {
                    vm.transactionTypeList = response.data;
                });
        }

        function getReasonLookup() {
            lookupService.get('reason')
                .then(function (response) {
                    vm.reasonList = response.data;
                });
        }

        function searchInsured(query) {
            lookupService.get('insured', query)
                .then(function (response) {
                    vm.insuredList = response.data;
                });
        }

        function searchBroker(query) {
            lookupService.get('broker', query)
                .then(function (response) {
                    vm.brokerList = response.data;
                });
        }

        function setInsured() {
            vm.insured = _.find(vm.insuredList, function (o) { return o.id == vm.policy.insuredId; });
        }

        function setBroker() {
            vm.broker = _.find(vm.brokerList, function (o) { return o.id == vm.policy.brokerId; });
        }

        function checkStatus() {
            vm.status = _.find(vm.statusList, function (o) { return o.id == vm.policy.statusId; });
            vm.disableReason = vm.status.value.indexOf('Cancelled') < 0 && vm.status.value.indexOf('Declined') < 0;
            vm.hideReasonDiv = vm.disableReason;
            //clear reason if status is not cancelled/declined.
            if (vm.disableReason) {
                vm.policy.reasonId = null;
            }
        }

        function populateEndDate() {
            var dateToObj = moment(vm.policy.periodOfCoverFrom, "DD/MM/YYYY").toDate();
            dateToObj.setFullYear(dateToObj.getFullYear() + 1);
            vm.policy.periodOfCoverTo = moment(dateToObj).format('DD/MM/YYYY');
        }

        $scope.$on('$viewContentLoaded', function () {
            vm.init();
        });
    }

})(angular.module('Epsilon'));
