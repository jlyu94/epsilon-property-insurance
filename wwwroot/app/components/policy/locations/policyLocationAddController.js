﻿(function (module) {
  'use strict';

  module.controller('policyLocationAddController', ['$rootScope', '$scope', '$state', '$timeout', 'authService', '$location', 'errorService', 'moment', '$localStorage', 'policyService', 'lookupService', 'settings', policyLocationAddController]);

  function policyLocationAddController($rootScope, $scope, $state, $timeout, authService, $location, errorService, moment, $localStorage, policyService, lookupService, settings) {
    var vm = this;
    vm.location = {};
    vm.burglaryProtectionList = [];
    vm.constructionCategoryList = [];
    vm.fireBrigadeList = [];
    vm.fireDetectionList = [];
    vm.fireProtectionList = [];
    vm.floorList = [];
    vm.indemnityPeriod = [];
    vm.occupationList = [];
    vm.roofList = [];
    vm.wallsInteriorList = [];
    vm.wallsExteriorList = [];
    vm.waterAdequacyList = [];
    vm.waterSupplyTypeList = [];
    vm.yesNoList = [{ id: 0, value: 'No' }, { id: 1, value: 'Yes' }, ];
    vm.save = save;
    vm.saveAndAddAnother = saveAndAddAnother;
    vm.cancel = cancel;
    vm.searchOccupation = searchOccupation;
    getBurglaryProtectionLookup();
    getConstructionCategoryLookup();
    getFireBrigadeLookup();
    getFireDetectionLookup();
    getFireProtectionLookup();
    getFloorLookup();
    getIndemnityPeriodLookup();
    getoccupationLookup();
    getRoofLookup();
    getWallsInteriorLookup();
    getWallsExteriorLookup();
    getWaterAdequacyLookup();
    getWaterSupplyTypeLookup();

    vm.init = function () {
      vm.policyId = $stateParams.id;
    }

    function getBurglaryProtectionLookup() {
      lookupService.get('burglaryProtection')
          .then(function (response) {
            vm.burglaryProtectionList = response.data;
          });
    }

    function getConstructionCategoryLookup() {
      lookupService.get('constructionCategory')
          .then(function (response) {
            vm.constructionCategoryList = response.data;
          });
    }

    function getFireBrigadeLookup() {
      lookupService.get('fireBrigade')
          .then(function (response) {
            vm.fireBrigadeList = response.data;
          });
    }

    function getFireDetectionLookup() {
      lookupService.get('fireDetection')
          .then(function (response) {
            vm.fireDetectionList = response.data;
          });
    }

    function getFireProtectionLookup() {
      lookupService.get('fireProtection')
          .then(function (response) {
            vm.fireProtectionList = response.data;
          });
    }

    function getFloorLookup() {
      lookupService.get('floor')
          .then(function (response) {
            vm.floorList = response.data;
          });
    }

    function getIndemnityPeriodLookup() {
      lookupService.get('indemnityPeriod')
          .then(function (response) {
            vm.indemnityPeriodList = response.data;
          });
    }

    function getoccupationLookup() {
      lookupService.get('occupation')
          .then(function (response) {
            vm.occupationList = response.data;
          });
    }

    function getRoofLookup() {
      lookupService.get('roof')
          .then(function (response) {
            vm.roofList = response.data;
          });
    }

    function getWallsInteriorLookup() {
      lookupService.get('wallsInterior')
          .then(function (response) {
            vm.wallsInteriorList = response.data;
          });
    }

    function getWallsExteriorLookup() {
      lookupService.get('wallsExterior')
          .then(function (response) {
            vm.wallsExteriorList = response.data;
          });
    }

    function getWaterAdequacyLookup() {
      lookupService.get('waterAdequacy')
          .then(function (response) {
            vm.waterAdequacyList = response.data;
          });
    }

    function getWaterSupplyTypeLookup() {
      lookupService.get('waterSupplyType')
          .then(function (response) {
            vm.waterSupplyTypeList = response.data;
          });
    }

    function save(isValid) {
      if (isValid) {
        settings.policy.defaultTab = 1;
        $state.go('policy-details', { id: $state.params.id });  
      }
    }

    function saveAndAddAnother(isValid) {
      if (isValid) {
        $state.reload();
      }
    }

    function cancel() {
      settings.policy.defaultTab = 1;
      $state.go('policy-details', { id: $state.params.id });
    }

    function searchOccupation(query) {
      lookupService.get('occupation', query)
          .then(function (response) {
            vm.occupationList = response.data;
          });
    }
  }
})(angular.module('Epsilon'));

