﻿(function (module) {
    'use strict';

    module.controller('policyController', ['$rootScope', '$scope', '$state', '$timeout', 'authService', '$location', 'errorService', 'moment', '$localStorage', 'policyService', 'lookupService', 'settings', policyController]);

    function policyController($rootScope, $scope, $state, $timeout, authService, $location, errorService, moment, $localStorage, policyService, lookupService, settings) {
      var vm = this;
      settings.policy.defaultTab = 0;
        
        vm.init = function () {
            vm.filters = {};
            vm.searchBroker = searchBroker;

            lookupService.get('status').then(function(response){
                vm.statuses = response.data;
            });

            lookupService.get('underwriter').then(function(response){
                vm.underwriters = response.data;
            });

            lookupService.get('broker').then(function (response) {
                vm.brokers = response.data;
            });
            
        }

        function searchBroker(query) {
            lookupService.get('broker', query)
                .then(function (response) {
                    vm.brokerList = response.data;
                });
        }

        vm.search = function () {
            $('#policyTable').bootstrapTable('refresh', { pageNumber: 1 });
        }

        vm.clear = function () {
            vm.filters = {};
            vm.search();
        }

        var formatter = function (value) {
            return '<a class="btn btn-outline btn-sm green-jungle" href="#/policy/' + value + '"><i class="fa fa-eye"></i> View</a>';
        }

        var queryParams = function (params) {
            return params;
        }

        vm.table = {
            url: ApiURL + '/policies',
            params: queryParams,
            columns: [
                { field: 'refNo', title: 'Ref No', sortable: true },
                { field: 'insuredName', title: 'Insured Name', sortable: true },
                { field: 'quoteNo', title: 'Quote No', sortable: true },
                { field: 'policyNo', title: 'Policy No', sortable: true },
                { field: 'inceptionDate', title: 'Inception Date', sortable: true },
                { field: 'type', title: 'Type', sortable: true },
                { field: 'status', title: 'Status', sortable: true },
                { field: 'underwriter', title: 'Underwriter', sortable: true },
                { field: 'broker', title: 'Broker', sortable: true },
                { field: 'id', title: '', sortable: false, formatter: formatter, width: '10px' }
            ],
            events: function () { }
        };

        $scope.$on('$viewContentLoaded', function () {
            vm.init();
        });
    }
})(angular.module('Epsilon'));

