﻿(function (module) {
    'use strict';

    module.controller('policyDetailsController', ['$rootScope', '$scope', '$state', '$timeout', 'authService', '$location', 'errorService', 'moment', '$localStorage', '$stateParams', 'policyService', 'lookupService', 'tableUtilsService', 'settings', '$uibModal', policyDetailsController]);

    function policyDetailsController($rootScope, $scope, $state, $timeout, authService, $location, errorService, moment, $localStorage, $stateParams, policyService, lookupService, tableUtilsService, settings, $uibModal) {
      var vm = this;
      vm.activeTab = 0;
      vm.sublimit = {};
      vm.yesNoList = [{ id: 0, value: 'No' }, { id: 1, value: 'Yes' }, ];

        vm.init = function () {
            vm.id = $stateParams.id;
            vm.isPolicyDetails = true;
            vm.save = save;
            vm.actionList = ['Action 1', 'Action 2', 'Action 3'];
            vm.loadLocations = loadLocations;
            vm.addLocation = addLocation;
            getBrokerLookup();
            getInsuredLookup();
            getStatusLookup()
            getUnderwriterLookup()
            getTransactionTypeLookup();
            getReasonLookup();
            vm.activeTab = settings.policy.defaultTab;
            vm.editMemoranda = editMemoranda;
            $timeout(function () {
                getPolicy();
            }, 1000);

            if (vm.activeTab == 1) {
              loadLocations();
            }

        }

        function save() {
            $state.go('policy');
        }

        function getPolicy() {
            policyService.get(vm.id)
                .then(function (response) {
                    vm.policy = response.data;
                    console.log(vm.policy);
                    vm.insured = getInsured(vm.policy.insuredId);
                    vm.broker = getBroker(vm.policy.brokerId);
                    vm.holdingBroker = getBroker(vm.policy.holdingBrokerId);
                    vm.holdingInsurer = getInsured(vm.policy.holdingInsurerId);
                    vm.transactionType = getTransactionType(vm.policy.transactionTypeId);
                    vm.periodOfCover = tableUtilsService.dateFormatter(vm.policy.periodOfCoverFrom) + " - " + tableUtilsService.dateFormatter(vm.policy.periodOfCoverTo);
                    vm.effectivityDates = tableUtilsService.dateFormatter(vm.policy.effectivityFrom) + " - " + tableUtilsService.dateFormatter(vm.policy.effectivityTo);
                    vm.status = formatStatus(getStatus(vm.policy.statusId));
                    vm.underwriter = getUnderwriter(vm.policy.underwriterId);
                });
        }

        function getStatusLookup() {
            lookupService.get('status')
                .then(function (response) {
                    vm.statusList = response.data;
                });
        }

        function getUnderwriterLookup() {
            lookupService.get('underwriter')
                .then(function (response) {
                    vm.underwriterList = response.data;
                });
        }

        function getTransactionTypeLookup() {
            lookupService.get('transactiontype')
                .then(function (response) {
                    vm.transactionTypeList = response.data;
                });
        }

        function getReasonLookup() {
            lookupService.get('reason')
                .then(function (response) {
                    vm.reasonList = response.data;
                });
        }

        function getInsuredLookup() {
            lookupService.get('insured')
                .then(function (response) {
                    vm.insuredList = response.data;
                });
        }

        function getBrokerLookup() {
            lookupService.get('broker')
                .then(function (response) {
                    vm.brokerList = response.data;
                });
        }

        function getInsured(id) {
            return _.find(vm.insuredList, function (o) { return o.id == id; });
        }

        function getBroker(id) {
            return _.find(vm.brokerList, function (o) { return o.id == id; });
        }

        function getTransactionType(id) {
            return _.find(vm.transactionTypeList, function (o) { return o.id == id; });
        }

        function getStatus(id) {
            return _.find(vm.statusList, function (o) { return o.id == id; });
        }

        function formatStatus(status) {
            var formatted = status.value;
            if (status.value) {
                var colorClass = getStatusColorClass(status.value);
                var statusArr = status.value.split(" - ");
                var subStatus = statusArr.length > 1 ? '<i class="text-' + colorClass + '">' + statusArr[1] + '</i>' : '';
                formatted = '<span class="label label-' + colorClass + '">' + statusArr[0] + '</span> ' + subStatus;
                return formatted;
            }

            return formatted;
        }

        function getStatusColorClass(value) {

            var colorClass = "default";
            if (value.indexOf("Cancelled") > -1 || value.indexOf("Declined") > -1 || value.indexOf("Lost") > -1 || value.indexOf("Lapsed") > -1)
                colorClass = "danger";
            else if(value.indexOf("Pending") > -1)
                colorClass = "warning";
            else if(value.indexOf("Quoted") > -1 )
                colorClass = "info"
            else if(value.indexOf("Bound") > -1 )
                colorClass = "success"

            return colorClass;
        }

        function getUnderwriter(id) {
            return _.find(vm.underwriterList, function (o) { return o.id == id; });
        }

        function loadLocations() {

          var policyFormatter = function (value) {

            return '<a class="btn btn-outline btn-sm green-jungle" href="#/policy/' + vm.id + '"><i class="fa fa-pencil"></i> Edit</a>'
              + '<a class="btn btn-outline btn-sm red-intense" href="#/policy/' + vm.id + '"><i class="fa fa-trash"></i> Delete</a>';
          }

          var currencyFormatter = function (value) {
            var valueInNumber = parseFloat(value);
            return (valueInNumber).toLocaleString('en-AU', { style: 'currency', currency: 'AUD' });
          }

          var boolFormatter = function (value) {
            var valueInBoolean = value == 'true'
            return valueInBoolean ? 'Yes' : 'No';
          }

          vm.policyLocationTable = {
            url: ApiURL + '/policies/' + vm.id + '/location',
            columns: [
                { field: 'refNo', title: 'Ref No', sortable: true, align: 'right' },
                { field: 'situation', title: 'Situation', sortable: true },
                { field: 'pCode', title: 'P\'Code', sortable: true, align: 'right' },
                { field: 'occupation', title: 'Occupation', sortable: true },
                { field: 'propertyOwnerOnly', title: 'Property Owner Only', sortable: true, formatter: boolFormatter },
                { field: 'section1', title: 'Section 1 Values', sortable: true, align: 'right', formatter: currencyFormatter },
                { field: 'section2', title: 'Section 2 Values', sortable: true, align: 'right', formatter: currencyFormatter },
                { field: 'totalDV', title: 'Total DV', sortable: true, align: 'right', formatter: currencyFormatter },
                { field: 'indemnityPeriod', title: 'Indemnity Period', sortable: true, align: 'right' },
                { field: 'constructionYear', title: 'Construction Year', sortable: true, align: 'right' },
                { field: 'constructionCategory', title: 'Construction Category', sortable: true, align: 'right' },
                { field: 'wallsInterior', title: 'Walls Interior', sortable: true, align: 'right' },
                { field: 'wallsExterior', title: 'Walls Exterior', sortable: true, align: 'right' },
                { field: 'roof', title: 'Roof', sortable: true, align: 'right' },
                { field: 'floor', title: 'Floor', sortable: true, align: 'right' },
                { field: 'fireProtection', title: 'Fire Protection', sortable: true, align: 'right' },
                { field: 'fireDetection', title: 'Fire Detection', sortable: true, align: 'right' },
                { field: 'fireBrigade', title: 'Fire Brigade', sortable: true, align: 'right' },
                { field: 'burglaryProtection', title: 'Burglary Protection', sortable: true, align: 'right' },
                { field: 'waterSupplyType', title: 'Water Supply Type', sortable: true, align: 'right' },
                { field: 'waterAdequacy', title: 'Water Adequacy', sortable: true, align: 'right' },
                { field: 'id', title: '', sortable: false, formatter: policyFormatter, width: '11%', align: 'center' }
            ],
            events: function () { }
          };
        }

        function addLocation() {
          $state.go('policy-location-add', { id: vm.id });
        }

        function editMemoranda(heading) {
          $scope.heading = heading;
          var editMemorandaModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/components/policy/sublimits/memoranda.html',
            controller: 'memorandaController',
            controllerAs: 'vm',
            backdrop: 'static',
            scope: $scope,
            windowClass: 'center-modal',
            keyboard: false
          });

          editMemorandaModalInstance.result.then(function (status) { },
          function (closeStatus) {
            if (closeStatus === 'save') {
              settings.isBusy = true;
            }
          });
        }

        $scope.$on('$viewContentLoaded', function () {
            vm.init();
        });
    }

})(angular.module('Epsilon'));
