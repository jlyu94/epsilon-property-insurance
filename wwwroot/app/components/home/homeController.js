﻿(function (module) {
    'use strict';

    module.controller('homeController', ['$rootScope', '$scope', '$state', '$timeout', 'authService', '$location', 'errorService', 'moment', '$localStorage', homeController]);

    function homeController($rootScope, $scope, $state, $timeout, authService, $location, errorService, moment, $localStorage) {
        var vm = this;
        
        vm.init = function () {
            
        }

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            vm.init();
        });
    }
})(angular.module('Epsilon'));
