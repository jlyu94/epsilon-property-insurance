﻿(function (module) {
    'use strict';

    module.controller('homeFilterController', ['$rootScope', '$scope', '$state', '$stateParams', '$timeout', 'authService', 'odContractService', 'permissionService', 'xeroService', '$location', 'errorService', 'moment',
        function ($rootScope, $scope, $state, $stateParams, $timeout, authService, odContractService, permissionService, xeroService, $location, errorService, moment) {
        var vm = this;
        
        var queryParams = function (params) {
            params.$count = true;
            params.$top = params.limit;
            params.$skip = params.offset;
            if (params.sort) {
                params.$orderby = params.sort + " " + params.order;
            }
            params.$select = "id,jobTitle,startDate, employee, client, contractStatus";
            params.$expand = "employee($select=firstName,lastName),client($select=name), contractStatus($select=name)";

            for (var i in params) {
                if (!i.startsWith('$'))
                    delete params[i];
            }

            if (vm.filters.length > 0)
                params.$filter = vm.filters;

            return params;
        }

        vm.init = function () {
            vm.exportToPayrollCount = 0;
            vm.approvalCount = 0;
            vm.signingCount = 0;
            vm.draftCount = 0;
            vm.filters = [];
            vm.permission = permissionService;
            vm.items = [];
            var currentUser = authService.getCurrentUser();

            vm.activeTable = $stateParams.filter;
            vm.activeTable = vm.activeTable || 'draft';
            var allowableFilters = ['draft', 'signing', 'approval', 'payroll'];

            if (allowableFilters.indexOf(vm.activeTable) < 0)
                $state.go('home');

            $('#pendingListTable').bootstrapTable();
            vm.setActiveTable(vm.activeTable);

            var actionEvents = {
                'click .contract-edit': function (e, value, row, index) {
                    $state.go('contracts-edit', { id: row.id });
                },
                'click .contract-view': function (e, value, row, index) {
                    $state.go('contract', { id: row.id });
                }
            };

            vm.table = {
                cache: false,
                url: ApiURL + 'odata/contracts/',
                params: queryParams,
                ajaxOptions: function () { return { headers: { "Authorization": "Bearer " + $rootScope.AccessToken() } } },
                responseHandler: function (response) {
                    return { total: response['@odata.count'], rows: response["value"] };
                },
                columns: [
                    {
                        field: 'client/name',
                        sortable: true,
                        visible: authService.hasRoles(["Administrator"]),
                        title: 'Client',
                        formatter: function (field, row) { return row.client.name }
                    },
                    {
                        field: 'employee/firstName',
                        sortable: true,
                        title: 'Employee',
                        formatter: function (field, row) { return row.employee.firstName + ' ' + row.employee.lastName }
                    },
                    {
                        field: 'jobTitle',
                        sortable: true,
                        title: 'Job Title'
                    },
                    {
                        field: 'startDate',
                        sortable: true,
                        title: 'Commencement',
                        align: 'center',
                        formatter: function (field, row) {
                            var startDate = moment(row.startDate, 'YYYY-MM-DDT00:00:00.000Z').format('DD/MM/YYYY');
                            return startDate;
                        }
                    },
                    {
                        field: 'Status/name',
                        sortable: true,
                        title: 'Status',
                        formatter: function (field, row) { return row.contractStatus.name }
                    },
                    {
                        field: 'id',
                        title: '',
                        formatter: contractFormatter,
                        events: actionEvents,
                        class: 'text-center',
                        width: '10%'
                    },
                ]
            }

        }

        function contractFormatter(field, row, index) {
            var editButton = '<a class="btn btn-outline btn-sm green-jungle contract-edit" title="Edit" href="/#/contracts/edit/' + row.id + '"><i class="glyphicon glyphicon-edit"></i>&nbsp;edit</a>';
            var viewButton = '<a class="btn btn-outline btn-sm green-jungle contract-view" title="View" href="/#/contracts/view/' + row.id + '"><i class="glyphicon glyphicon-eye-open"></i>&nbsp;view</a>';

            if (!vm.permission.hasPermission('viewcontracts')) {
                viewButton = "";
            }
            if (!vm.permission.hasPermission('initiateandeditcontracts')) {
                editButton = "";
            }
            
            return viewButton + editButton;
        }

        vm.isActiveTable = function (table) {
            return vm.activeTable === table;
        };

        vm.setActiveTable = function (table) {
            vm.activeTable = table;
            switch (table) {
                case 'draft':
                    vm.filters = [];
                    var filters = [];
                    filters.push("contractStatusID eq '1'");
                    if (filters.length > 0) 
                        vm.filters = filters.join(" and ");
                    $('#pendingLists').bootstrapTable('refresh', { pageNumber: 1 });
                    break;
                case 'signing':
                    vm.filters = [];
                    var filters = [];
                    filters.push("contractStatusID eq '2'");
                    filters.push(
                        "(documents/any(i:((i/clientSigned eq false) or (i/clientSignRequired eq true))) eq true)");
                    if (filters.length > 0) {
                        vm.filters = filters.join(" and ");
                    }
                    $('#pendingLists').bootstrapTable('refresh', { pageNumber: 1 });
                    break;
                case 'approval':
                    vm.filters = [];
                    var filters = [];
                    filters.push("contractStatusID eq '3'");
                    if (filters.length > 0) {
                        vm.filters = filters.join(" and ");
                    }
                    $('#pendingLists').bootstrapTable('refresh', { pageNumber: 1 });
                    break;
                case 'payroll':
                    {
                        vm.filters = [];
                        var filters = [];
                        filters.push("contractStatusID eq '4'");
                        filters.push("employee/keyPayExportStatusID ne 2");
                        filters.push("employee/keyPayEmployeeID eq null");

                        if (filters.length > 0) {
                            vm.filters = filters.join(" and ");
                        }
                        $('#pendingLists').bootstrapTable('refresh', { pageNumber: 1 });
                    }
                    break;
            }
        };

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            vm.init();
        });

    }]);
})(angular.module('Epsilon'));
