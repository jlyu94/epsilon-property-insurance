﻿(function (module) {
    'use strict';

    module.controller('loginController', ['$rootScope', '$scope', '$state', '$stateParams', '$location', '$timeout', 'authService', 'contractNotificationKeyService', 'userService', function ($rootScope, $scope, $state, $stateParams, $location, $timeout, authService, contractNotificationKeyService, userService) {
        var vm = this;
        vm.login = {};
        vm.auth = authService;

        if (authService.isLoggedIn()) {
            $state.go('home');
        };

    }]);
})(angular.module('Epsilon'));
