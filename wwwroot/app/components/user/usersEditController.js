(function (module) {
    'use strict';

    module.controller('usersEditController', ['$rootScope', '$scope', '$timeout', '$stateParams', 'userService', '$log', '$http', '$state','clientService','$filter', 'lookupService',
    function ($rootScope, $scope, $timeout, $stateParams, userService, $log, $http, $state,clientService,$filter, lookupService) {
            var vm = this;
            var id = $stateParams.id;
            vm.id = $stateParams.id;
            vm.treeInstance = {};
            vm.treeInstanceOnboarding = {};
            vm.user = {};
            vm.isBusy = true;
            vm.errorMessage = "";
            vm.treeData = [];
            vm.treeDataOnboarding = [];
            vm.saveErrors = [];
            vm.isNew = false;
            vm.isUISelect = true;
            vm.filterObject = { clientId: null, clientDepartmentId: null, clientSiteId: null, clientUserLists: [] };
            vm.getUser = function () {
                var id = $stateParams.id;
                userService.find(id)
                    .then(
                        function (response) {
                            vm.user = response.data;

                        },
                        function (data) {
                            console.log(data);
                        })
                        .finally(function () {
                            vm.loadTimezones();
                        });


            };

            vm.editUser = function () {
                vm.saving = true;
 
                userService.update(vm.user, id)
                .then(
                    function (response) {
                        vm.saving = false;
                        $state.go('users');
                    },
                    function (data) {
                        vm.saving = false;
                        vm.saveErrors.push(data.data);
                    })
            }

            vm.getUser();

            vm.loadTimezones = function () {
                lookupService.get("Timezone").then(
                      function (response) {
                          vm.timezones = response.data;
                      });
            }
        }]);
})(angular.module('Epsilon'));