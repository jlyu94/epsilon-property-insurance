(function (module) {
    'use strict';

    module.controller('usersController', ['$rootScope', '$scope', '$timeout', 'userService', function ($rootScope, $scope, $timeout, userService) {
        var vm = this;
        vm.user = {};

        vm.users = [];
        vm.isBusy = true;
        vm.errorMessage = "";

        vm.search = function () {
            $('#userTable').bootstrapTable('selectPage', 1);
        }

        vm.clear = function () {
            vm.user = {};
            vm.search();
        }

        var formatter = function (value) {
            return '<a class="btn btn-outline btn-sm green-jungle" href="/#/users/' + value + '"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        }

        var checkbox = function (value) {
            if (value == true) {
                return '<input type="checkbox" name="inactive" id="inactive" checked="checked" disabled="disabled">';
            }
            else {
                return '<input type="checkbox" name="inactive" id="inactive" disabled="disabled">';
            }
            
        }

        var queryParams = function (params) {
            params.FullName = vm.user.FullName;
            params.Email = vm.user.Email;
            params.Inactive = vm.user.Inactive;
            return params;
        }

        vm.table = {
            url: ApiURL + 'api/user',
            params: queryParams,
            columns: [
                { field: 'email', title: 'Username', sortable: true },
                { field: 'firstName', title: 'Firstname', sortable: true },
                { field: 'lastName', title: 'Lastname', sortable: true },
               // { field: 'inactive', title: 'Inactive', sortable: false, formatter:checkbox, width: '20%' },
                { field: 'id', title: '', sortable: false, formatter: formatter, width: '10px' }
            ],
            events: function () { }
        };
    }]);
})(angular.module('Epsilon'));