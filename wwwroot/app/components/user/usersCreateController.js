(function (module) {
    'use strict';

    module.controller('usersCreateController', ['$rootScope', '$state','$scope', '$timeout', '$stateParams', 'userService', 'clientService','$filter', 'lookupService',
    function ($rootScope, $state, $scope, $timeout, $stateParams, userService, clientService,$filter, lookupService) {
            var vm = this;
            vm.user = { clients: [] };

            vm.saveErrors = [];
            vm.isNew = true;
            vm.createMode = true;
            vm.isUISelect = true;
            vm.filterObject = { clientId: null, clientDepartmentId: null, clientSiteId: null, clientUserLists: [] };
            vm.addUser = function () {
                vm.saving = true;
                userService.create(vm.user).then(
                    function (response) {
                        vm.saving = false;
                        //success
                        var id = response.data;
                        $state.go('users');
                        
                    },
                    function (response) {
                        vm.saving = false;
                        //error
                        vm.saveErrors.push(response.data);
                    });
            }

            vm.loadTimezones = function () {
                lookupService.get("Timezone").then(
                      function (response) {
                          vm.timezones = response.data;
                          vm.user.timezoneID = $rootScope.epConstants.timezones.default;
                      });
            }

            vm.loadTimezones();

        }]);
})(angular.module('Epsilon'));