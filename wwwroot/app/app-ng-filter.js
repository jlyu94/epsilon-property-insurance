﻿(function (app) {

    "use strict";

    app.filter("trust", ['$sce', function ($sce) {
        return function (htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        }
    }]);

    app.filter("trustUrl", ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        }
    }]);

    app.filter("splitCamelCase", [function () {
        return function (word) {
            return _.startCase(word);
        }
    }]);

    app.filter("listValue", ['$sce', function ($sce) {
        return function (id, list) {
            if (id && list) {
                for (var i = 0; i < list.length; i++) {
                    var item = list[i];
                    if (item.id == id || item.value == id) {
                        return item.name;
                    }
                }
            }
            return "";
        }
    }]);

})(angular.module('Epsilon'))